function search_response_handler(data) {
    $('#results').html(data);
}

function type_handler() {
    var filter_type = $('#filter-type').val();
    var query = $('#formGroupInputLarge').val();
    $.get(
        "/search/?type=" + filter_type + "&query=" + query,
        search_response_handler
    );
}

$('#formGroupInputLarge').on('input', type_handler);

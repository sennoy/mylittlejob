To setup project create virualenv with command:

$ virtualenv dev_venv --python=/path/to/python/python3.5

activate virtual env with:

$ source dev_venv/bin/activate

install requirements with:

$ pip install -r requirements.txt

now, run tornado server with following command:

$ python server.py


by default it will bind to port 7777, so you need to open url http://localhost:7777
you can bind it to another port changing appropriate setting in dev.settings.base module

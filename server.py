"""
it would be better if this file will be moved to
dev.web.tornado
but I decided to leave it here to simplify setup process
"""

import tornado.ioloop
import tornado.web
from tornado.httpserver import HTTPServer

from dev.settings import get_settings, from_env
from dev.web.tornado import handlers


def make_app():
    return tornado.web.Application(
        handlers.gather(),
        **get_settings().Tornado.settings
    )


def run_server():
    from_env()
    http_server = HTTPServer(make_app())
    http_server.listen(get_settings().Tornado.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    run_server()

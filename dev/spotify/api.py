from dev.settings import get_settings
from dev.web.utils.async import fetch_json


async def search(query, type):
    result = await fetch_json(
        get_settings().Spotify.search_url.format(
            query=query,
            search_type=type,
        )
    )
    return result


def items(data, spotify_type):
    return data[_response_type(spotify_type)]['items']


def _response_type(spotify_type):
    return get_settings().Spotify.spotify_types.get(spotify_type)

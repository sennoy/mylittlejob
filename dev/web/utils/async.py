import json

from tornado.httpclient import AsyncHTTPClient


async def fetch(*args, **kwargs):
    return await AsyncHTTPClient().fetch(
        *args, **kwargs
    )


async def fetch_json(*args, **kwargs):
    return json.loads(
        (await fetch(*args, **kwargs)).body.decode()
    )

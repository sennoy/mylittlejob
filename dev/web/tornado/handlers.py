import tornado.web

from dev.settings import get_settings
from dev.web.tornado.spotify.handlers import SpotifySearchHandler


def gather():
    Static = get_settings().Static
    return [
        (r"/search/", SpotifySearchHandler),
        (r"/(.*)", tornado.web.StaticFileHandler, {
            'path': Static.static_path,
            'default_filename': Static.default_filename
        }),
    ]

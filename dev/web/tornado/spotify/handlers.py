import tornado.web

from dev.jinja.spotify.search import render_search_result
from dev.spotify import api as spotify_api


class SpotifySearchHandler(tornado.web.RequestHandler):

    async def get(self):
        search_name = self.get_argument('query')
        search_type = self.get_argument('type')
        if not search_name:
            # no query - clear existing results
            result = []
        else:
            result = spotify_api.items(
                await spotify_api.search(search_name, search_type),
                search_type
            )

        rendered = render_search_result(result)
        self.write(rendered)

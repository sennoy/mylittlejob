

class Settings:
    # this settings schema allows to use native python inheritance
    # to setup settings for different servers/developers

    class Tornado:
        port = 7777
        settings = {
            'debug': True,
        }

    class Spotify:
        search_url = (
            'https://api.spotify.com/v1/'
            'search?q={query}&type={search_type}'
        )
        spotify_types = {
            'artist': 'artists',
            'album': 'albums',
            'playlist': 'playlists',
            'track': 'tracks',
        }

    class Static:
        static_path = 'static/'
        default_filename = 'html/index.html'

    class Jinja:
        app_name = 'dev'
        templates_dir = 'jinja/templates'

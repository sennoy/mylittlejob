import os
from importlib import import_module

settings = None


def get_settings():
    return settings


def from_class(settings_class):
    global settings
    settings = settings_class


def from_module_name(module_name):
    mod = import_module(module_name)
    from_class(mod.Settings)


def from_env():
    module = os.environ.get('SETTINGS_MODULE') or 'dev.settings.base'
    from_module_name(module)


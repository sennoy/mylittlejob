from jinja2 import Environment, PackageLoader

from dev.settings import get_settings


def _is_64_thumbnail(image):
    return image.get('height') == image.get('width') == 64


def _search_context(data, filter_img=None):
    # trick with filter_img can be used if we want to filter
    # different images for different cases, just need to pass appropriate func
    result = []
    for item in data:
        # all this stuff can be rewritten to 1-2 lines, BUT
        # multiple statements simplify debugging
        # and increase readability.
        name = item.get('name')
        image = list(
            filter(
                lambda img: filter_img(img),
                item.get('images') or []
            )
        )
        result.append({
            'name': name,
            'image': image[0].get('url') if image else '',
        })
    return result


def render_search_result(raw_context):
    # here input param can be added
    # something like image_size
    # then dict with something like funcs = {64: _is_64_thumbnail}
    # and _search_context(raw_context, funcs.get(image_size)
    context = _search_context(raw_context, _is_64_thumbnail)

    Jinja = get_settings().Jinja
    env = Environment(
        loader=PackageLoader(Jinja.app_name, Jinja.templates_dir),
    )
    template = env.get_template('spotify_search_result.md')
    return template.render(context=context)
